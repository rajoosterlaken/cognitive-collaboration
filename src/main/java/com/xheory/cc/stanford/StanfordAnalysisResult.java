package com.xheory.cc.stanford;

import com.xheory.cc.rest.sentiment.controllers.IResult;

import lombok.Data;

@Data
public class StanfordAnalysisResult implements IResult {
	
	private String sentence;
	private double[] coefficients = new double[5];
	private int predictionIndex;
	
	public StanfordAnalysisResult(String sentence, double[] coefficients, int predictionIndex) {
		this.sentence = sentence;
		this.predictionIndex = predictionIndex;
		this.coefficients = coefficients;
	}
	
}

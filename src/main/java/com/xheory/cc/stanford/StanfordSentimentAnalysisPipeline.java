package com.xheory.cc.stanford;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.ejml.simple.SimpleMatrix;
import org.springframework.stereotype.Component;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

@Component
public class StanfordSentimentAnalysisPipeline {

	private static final int OUTPUT_VECTOR_SIZE = 5;
	
	private StanfordCoreNLP pipeline;
	
	public StanfordSentimentAnalysisPipeline() {
		Properties properties = new Properties();
		properties.setProperty("annotators", "tokenize, ssplit, pos, parse, sentiment");
		pipeline = new StanfordCoreNLP(properties);
	}
	
	
	/**
	 * Analyse the sentiment of the given messages.
	 */
	public List<StanfordAnalysisResult> analyseBulk(List<String> messages) {
		List<StanfordAnalysisResult> results = new ArrayList<>();
		
		for (String message : messages) {
			List<StanfordAnalysisResult> subResults = (analyse(message));
			if (subResults.size() > 0) {
				results.add(subResults.get(0));
			}
		}
		
		return results;
	}
	
	
	/**
	 * Analyse the sentiment of the given message.
	 */
	public List<StanfordAnalysisResult> analyse(String message) {
		List<StanfordAnalysisResult> sentenceAnalysisResults = analyseSentences(message);
		
		if (sentenceAnalysisResults.size() > 1) {
			StanfordAnalysisResult finalResult = calculateTotalResult(message, sentenceAnalysisResults);
			sentenceAnalysisResults.add(0, finalResult);
		}
		
		return sentenceAnalysisResults;
	}
	
	
	/**
	 * Takes the analyzed sentences and calculates the result of the complete message. 
	 */
	private StanfordAnalysisResult calculateTotalResult(String message, List<StanfordAnalysisResult> sentenceAnalysisResults) {
		double[] finalCoefficients = calculateMean(sentenceAnalysisResults);
		int finalPrediction = indexOfMaximum(finalCoefficients);
		return new StanfordAnalysisResult(message, finalCoefficients, finalPrediction);
	}
	
	
	/**
	 * Analyse the sentiment of a message and return the results of the full message and per sub-sentence.
	 */
	private List<StanfordAnalysisResult> analyseSentences(String message) {
		Annotation document = toAnnotation(message);
		List<StanfordAnalysisResult> sentenceAnalysisResults = new ArrayList<StanfordAnalysisResult>();
		
		for (CoreMap sentence : document.get(SentencesAnnotation.class)) {
			StanfordAnalysisResult sentenceAnalysisResult = analyseSentence(sentence);
			sentenceAnalysisResults.add(sentenceAnalysisResult);
		}
		
		return sentenceAnalysisResults;
	}
	
	
	/**
	 * Transform message into an annotatable tree.
	 */
	private Annotation toAnnotation(String message) {
		Annotation document = new Annotation(message);
		pipeline.annotate(document);
		return document;
	}
	
	
	/**
	 * Analyse a core map, annotated sentence's sentiment.
	 */
	private StanfordAnalysisResult analyseSentence(CoreMap sentence) {
		Tree sentenceTree = sentence.get(SentimentAnnotatedTree.class);
		SimpleMatrix coefficientsMatrix = RNNCoreAnnotations.getPredictions(sentenceTree);
		double[] coefficients = vectorToArray(coefficientsMatrix);
		int predictedSentenceClass = RNNCoreAnnotations.getPredictedClass(sentenceTree);
		return new StanfordAnalysisResult(sentence.toString(), coefficients, predictedSentenceClass);
	}
	
	
	/**
	 * Transform a vector to a native double array. 
	 */
	private double[] vectorToArray(SimpleMatrix matrix) {
		double[] array = new double[OUTPUT_VECTOR_SIZE];
		
		for (int i = 0; i < OUTPUT_VECTOR_SIZE; i++) {
			array[i] = matrix.get(i);
		}
		
		return array;
	}
	
	
	/**
	 * Calculate the mean of the loose sentence sentiment vectors.
	 */
	private double[] calculateMean(List<StanfordAnalysisResult> results) {
		double[] means = new double[OUTPUT_VECTOR_SIZE];
		
		for (int i = 0; i < means.length; i++) {
			for (int j = 0; j < results.size(); j++) {
				means[i] += results.get(j).getCoefficients()[i];
			}
			means[i] /= results.size();
		}
		
		return means;
	}
	
	
	/**
	 * Find the index of the maximum value of the array.
	 */
	private int indexOfMaximum(double[] array) {
		int index = 0;
		double value = array[0];
		
		for (int i = 1; i < array.length; i++) {
			if (array[i] > value) {
				index = i;
				value = array[i];
			}
		}
		
		return index;
	}
	
}

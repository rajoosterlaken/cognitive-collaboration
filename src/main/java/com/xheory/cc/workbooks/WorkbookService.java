package com.xheory.cc.workbooks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xheory.cc.rest.sentiment.controllers.IResult;
import com.xheory.cc.rest.sentiment.controllers.dto.AnalysisResult;

@Service
public class WorkbookService {

	public static final String SHEETS_FOLDER = "." + File.separator + "sheets";
	public static final String UPLOAD_FOLDER = "." + File.separator + "uploads";
	
	private static final int TEXT_COLUMN_INDEX = 6;
	
	private static final Logger logger = LoggerFactory.getLogger(WorkbookService.class);
	private static final int SENTENCE_COLUMN_WIDTH = 64 * 256;
	private static final int DEFAULT_COLUMN_WIDTH = 13;
	private static final String[] HEADERS = {
			"Sentence",
			"Very Negative",
			"Negative",
			"Neutral",
			"Positive",
			"Very Positive"
	};
	
	
	public WorkbookService() {
		createDirectory(SHEETS_FOLDER);
		createDirectory(UPLOAD_FOLDER);
	}
	
	
	/**
	 * Create a results directory, if non-existent.
	 */
	public void createDirectory(String path) {
		File file = new File(path);
		
		if (!file.exists()) {
			file.mkdir();
		}
	}
	
	
	/**
	 * Save a workbook based on all given sentiment analysis results.
	 */
	public UUID saveToNewWorkbook(List<AnalysisResult> analysisResults) {
		UUID uuid = UUID.randomUUID();
		String workbookPath = SHEETS_FOLDER + File.separator + uuid + ".xlsx";
		XSSFWorkbook workbook = createWorkbook(analysisResults);
		
		try {
			FileOutputStream fileOut = new FileOutputStream(workbookPath);
			workbook.write(fileOut);
			fileOut.close();
			logger.info("A workbook has been saved to '" + workbookPath + "'.");
		} catch (Exception exception) {
			logger.error("An error occured while trying to write a workbook to file.", exception);
		}
		
		return uuid;
	}


	public UUID saveToExistingWorkbook(String filePath, List<AnalysisResult> analysisResults) {
		UUID uuid = UUID.randomUUID();
		String workbookPath = SHEETS_FOLDER + File.separator + uuid + ".xlsx";
		XSSFWorkbook existingWorkbook = loadWorkbook(filePath);
		XSSFWorkbook workbook = createWorkbook(existingWorkbook, WriteConfig.QUYNTESS_USE_CASE, analysisResults);
		
		try {
			FileOutputStream fileOut = new FileOutputStream(workbookPath);
			workbook.write(fileOut);
			fileOut.close();
			logger.info("A workbook has been saved to '" + workbookPath + "'.");
		} catch (Exception exception) {
			logger.error("An error occured while trying to write a workbook to file.", exception);
		}
		
		return uuid;
	}
	
	
	/**
	 * Wrapper in case no workbook was defined anyway.
	 */
	private XSSFWorkbook createWorkbook(List<AnalysisResult> analysisResults) {
		return createWorkbook(null, WriteConfig.DEFAULT, analysisResults);
	}
	
	
	/**
	 * Create a workbook based on the results of the sentiment analysis.
	 */
	private XSSFWorkbook createWorkbook(XSSFWorkbook workbook, WriteConfig config, List<AnalysisResult> analysisResults) {
		boolean usingExisting = workbook != null;
		
		if (!usingExisting) {
			workbook = new XSSFWorkbook();
			logger.info("New workbook instance created!");
		}
		
		StyleMap styleMap = new StyleMap(workbook);
		
		for (AnalysisResult analysisResult : analysisResults) {
			String sheetName = analysisResult.getModel().getDescription();
			List<? extends IResult> results = analysisResult.getResults();
			createSheet(workbook, config, styleMap, sheetName, results);
		}
		
		return workbook;
	}
	
	
	/**
	 * Create a worksheet for a single model of results.
	 */
	private void createSheet(XSSFWorkbook workbook, WriteConfig config, StyleMap styleMap, String sheetName, List<? extends IResult> results) {
		XSSFSheet sheet = config.isCopyingFromSource() ? workbook.cloneSheet(0, sheetName) : workbook.createSheet(sheetName);
		setColumnWidths(sheet, config);
		createHeaders(sheet, config, styleMap);
		int rowIndex = 1;
		
		for (IResult result : results) {
			createResultRow(sheet, config, styleMap, rowIndex++, result);
		}
	}
	
	
	/**
	 * Set the column widths of certain sheets.
	 */
	private void setColumnWidths(XSSFSheet sheet, WriteConfig config) {
		if (!config.isCopyingFromSource()) {
			sheet.setDefaultColumnWidth(DEFAULT_COLUMN_WIDTH);
			sheet.setColumnWidth(0, SENTENCE_COLUMN_WIDTH);
		}
	}
	
	
	/**
	 * Create the headers in the specified sheet.
	 */
	private void createHeaders(XSSFSheet sheet, WriteConfig config, StyleMap styleMap) {
		XSSFRow row = config.isCopyingFromSource() ? sheet.getRow(0) : sheet.createRow(0);
		int headerIndex = config.isWriteTextColumn() ? 0 : 1;
		int columnIndex = config.getStartingColumn();
		
		for (int i = headerIndex; i < HEADERS.length; i++) {
			XSSFCell cell = row.createCell(columnIndex++);
			cell.setCellValue(HEADERS[i]);
			styleMap.styleHeaderCell(cell);
		}
	}
	
	
	/**
	 * Create a row of results.
	 */
	private void createResultRow(XSSFSheet sheet, WriteConfig config, StyleMap styleMap, int rowIndex, IResult result) {
		XSSFRow row = config.isCopyingFromSource() ? sheet.getRow(rowIndex) : sheet.createRow(rowIndex);
		double[] coefficients = result.getCoefficients();
		int columnIndex = config.getStartingColumn();
		
		if (config.isWriteTextColumn()) {
			row.createCell(columnIndex++).setCellValue(result.getSentence());
		}
		
		for (int i = 1; i <= 5; i++) {
			int coefficientIndex = i - 1;
			XSSFCell cell = row.createCell(columnIndex++);
			cell.setCellValue(coefficients[coefficientIndex]);
			styleMap.styleResultCell(cell, coefficientIndex, result.getPredictionIndex() == coefficientIndex);
		}
	}
	
	
	/**
	 * Write a Multipartfile to the upload folder.
	 */
	public String writeAwayUpload(MultipartFile file) {
		byte[] bytes;
		try {
			bytes = file.getBytes();
			String filePath = WorkbookService.UPLOAD_FOLDER + File.separator + file.getOriginalFilename();
			Path path = Paths.get(filePath);
			Files.write(path, bytes);
			logger.info("A file has been uploaded to '" + path.toString() + "'.");
			return filePath;
		} catch (IOException e) {
			logger.error("An error occurred whilst uploading a workbook.", e);
			return null;
		}
	}
	
	
	/**
	 * Load the contents of the first row of a workbook and exports them as a list of strings.
	 */
	public List<String> extractMessagesFromWorkbook(String filePath) {
		XSSFWorkbook workbook = loadWorkbook(filePath);
		
		if (workbook.getNumberOfSheets() == 0) {
			logger.warn("Empty spreadsheet uploaded, nothing happened.");
			return null;
		}
		
		XSSFSheet sheet = workbook.getSheetAt(0);
		List<String> messages = new ArrayList<String>();
		int lastRowIndex = sheet.getLastRowNum() + 1;

		for (int rowIndex = 1; rowIndex <= lastRowIndex; rowIndex++) {
			XSSFRow row = sheet.getRow(rowIndex);
			
			if (row != null) {
				XSSFCell cell = row.getCell(TEXT_COLUMN_INDEX, MissingCellPolicy.RETURN_BLANK_AS_NULL);
				addCellToMessages(rowIndex, filePath, messages, cell);
			}
		}
		
		return messages;
	}


	/**
	 * Adds the value of the cell to the list of messages, or null when the value wasn't a string (or empty).
	 */
	private void addCellToMessages(int rowIndex, String filePath, List<String> results, XSSFCell cell) {
		try {		
			String string = cell.getStringCellValue();
			
			if (!string.isEmpty()) {
				results.add(string);
			} else {
				results.add(null);
				logger.warn("Row " + rowIndex + " (@ '" + filePath + "') had an empty value in the column.");
			}
		} catch(Exception e) {
			results.add(null);
			logger.warn("Row " + rowIndex + " (@ '" + filePath + "') had an invalid value in the column.");
		}
	}


	/**
	 * Load the workbook at the given path.
	 */
	private XSSFWorkbook loadWorkbook(String filePath) {
		XSSFWorkbook workbook = null;
		try {
			InputStream fileStream = new FileInputStream(filePath);
			workbook = new XSSFWorkbook(fileStream);
		} catch (IOException e) {
			logger.error("An error occurred whilst trying to extract workbook content.", e);
		}
		return workbook;
	}
	
}

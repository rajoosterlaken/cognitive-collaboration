package com.xheory.cc.workbooks;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WriteConfig {
	
	/**
	 * From which column index should the text started to get written.
	 */
	private int startingColumn;
	
	
	/**
	 * Should the text be written in front, or not.
	 */
	private boolean writeTextColumn;
	
	
	/**
	 * Whether or not the writer should copy the contents from the first sheet.
	 */
	private boolean copyingFromSource;
	
	
	public static final WriteConfig DEFAULT = new WriteConfig(0, true, false);
	public static final WriteConfig QUYNTESS_USE_CASE = new WriteConfig(11, false, true);
	
}

package com.xheory.cc.workbooks;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.xheory.cc.SentimentClass;


public class StyleMap {

	private static final XSSFColor BLACK = new XSSFColor(new Color(0, 0, 0));
	private static final XSSFColor WHITE = new XSSFColor(new Color(255, 255, 255));
	
	private XSSFWorkbook workbook;
	private XSSFFont headerFont, boldFont;
	private XSSFCellStyle headerStyle;
	private List<XSSFCellStyle> styleMap = new ArrayList<>();
	
	
	public StyleMap(XSSFWorkbook workbook) {
		this.workbook = workbook;
		initFonts();
		initHeaderCellStyle();
		initResultStyles();
	}
	
	
	/**
	 * Create the default fonts used in the results of the sheet.
	 */
	private final void initFonts() {
		headerFont = workbook.createFont();
		headerFont.setColor(WHITE);
		headerFont.setBold(true);
		boldFont = workbook.createFont();
		boldFont.setBold(true);
	}
	
	
	/**
	 * Create the default header style.
	 */
	private final void initHeaderCellStyle() {
		headerStyle = workbook.createCellStyle();
		headerStyle.setFillForegroundColor(BLACK);
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setFont(headerFont);
	}
	
	
	/**
	 * Create the default fonts used in the results of the sheet.
	 */
	private final void initResultStyles() {
		for (SentimentClass sentimentClass : SentimentClass.values()) {
			XSSFCellStyle defaultStyle = workbook.createCellStyle();
			defaultStyle.setDataFormat(workbook.createDataFormat().getFormat("0.0000"));
			defaultStyle.setFillForegroundColor(new XSSFColor(sentimentClass.getColorBrighter()));
			defaultStyle.setAlignment(HorizontalAlignment.CENTER);
			defaultStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			styleMap.add(defaultStyle);
			
			XSSFCellStyle winningStyle = workbook.createCellStyle();
			winningStyle.cloneStyleFrom(defaultStyle);
			winningStyle.setFillForegroundColor(new XSSFColor(sentimentClass.getColor()));
			winningStyle.setFont(boldFont);
			styleMap.add(winningStyle);
		}
	}
	
	
	/**
	 * Style the given cell as header.
	 */
	public void styleHeaderCell(XSSFCell cell) {
		cell.setCellStyle(headerStyle);
	}

	
	/**
	 * Style the given cell with the given parameters.
	 */
	public void styleResultCell(XSSFCell cell, int sentimentClassIndex, boolean isWinner) {
		int styleMapIndex = (sentimentClassIndex * 2) + (isWinner ? 1 : 0);
		cell.setCellStyle(styleMap.get(styleMapIndex));
	}
	
}

package com.xheory.cc;

import java.awt.Color;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SentimentClass {
	VERY_NEGATIVE	(new Color(255, 81, 81)),
	NEGATIVE		(new Color(255, 151, 81)),
	NEUTRAL			(new Color(255, 235, 81)),
	POSITIVE		(new Color(227, 253, 81)),
	VERY_POSITIVE	(new Color(145, 209, 81));

	@Getter
	private Color color;
	
	
	/**
	 * Returns the sentiment class by index, null if not found.  
	 */
	public static SentimentClass byOrdinal(int ordinal) {
		for (SentimentClass sentimentClass : values()) {
			if (sentimentClass.ordinal() == ordinal) {
				return sentimentClass;
			}
		}
		
		return null;
	}
	
	
	/**
	 * The value the color's RGB's are upped.
	 */
	private static final int BRIGHTNESS_VALUE = 16;

	
	/**
	 * Return the brighter variant of the color.
	 */
	public Color getColorBrighter() {
		int red = bound(color.getRed() + BRIGHTNESS_VALUE);
		int green = bound(color.getGreen() + BRIGHTNESS_VALUE);
		int blue = bound(color.getBlue() + BRIGHTNESS_VALUE);
		return new Color(red, green, blue);
	}
	
	
	/**
	 * Bind the input color value to the 0-255 range.
	 */
	private int bound(int value) {
		return value < 0 ? 0 : (value > 255 ? 255 : value); 
	}
}

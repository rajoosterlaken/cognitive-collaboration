package com.xheory.cc.rest.status.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xheory.cc.rest.status.APIStatus;

@Controller
public class APIStatusController {
	
	@RequestMapping(value="/status", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody APIStatus getStatus() {
		return new APIStatus();
	}
	
}

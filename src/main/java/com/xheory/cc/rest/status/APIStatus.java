package com.xheory.cc.rest.status;

import java.util.HashMap;
import java.util.Map;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class APIStatus {
	
	private Map<String, Boolean> status = new HashMap<>();
	
	public APIStatus() {
		this.status.put("Stanford NLP", true);
	}
	
}

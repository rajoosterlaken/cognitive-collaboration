package com.xheory.cc.rest.sentiment.controllers.dto;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SentimentAnalysisBulkResponse {
	
	private UUID download_id;
	private List<AnalysisResult> output;
	
}

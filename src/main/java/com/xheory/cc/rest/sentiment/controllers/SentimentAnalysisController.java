package com.xheory.cc.rest.sentiment.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.xheory.cc.rest.sentiment.ModelOption;
import com.xheory.cc.rest.sentiment.controllers.dto.AnalysisResult;
import com.xheory.cc.rest.sentiment.controllers.dto.ModelOptionsResponse;
import com.xheory.cc.rest.sentiment.controllers.dto.SentimentAnalysisBulkRequest;
import com.xheory.cc.rest.sentiment.controllers.dto.SentimentAnalysisBulkResponse;
import com.xheory.cc.rest.sentiment.controllers.dto.SentimentAnalysisRequest;
import com.xheory.cc.rest.sentiment.controllers.dto.UploadResponse;
import com.xheory.cc.stanford.StanfordAnalysisResult;
import com.xheory.cc.stanford.StanfordSentimentAnalysisPipeline;
import com.xheory.cc.workbooks.WorkbookService;

@Controller
@RequestMapping(value="/sentiment", produces="application/json")
public class SentimentAnalysisController {
	
	@Autowired
	private StanfordSentimentAnalysisPipeline stanfordService;
	
	@Autowired
	private WorkbookService workbookService;
	
	
	@RequestMapping(value="/models", method=RequestMethod.GET)
	public @ResponseBody ModelOptionsResponse requestModels() {
		return ModelOptionsResponse.get();
	}
	
	
	@RequestMapping(value="/analysis", method=RequestMethod.POST)
	public @ResponseBody List<AnalysisResult> analyseSentiment(@RequestBody SentimentAnalysisRequest request) {
		List<AnalysisResult> response = new ArrayList<AnalysisResult>();
		
		if (request.getModels().contains(ModelOption.STANFORD_NLP_DEFAULT)) {
			List<StanfordAnalysisResult> result = stanfordService.analyse(request.getMessage());
			response.add(new AnalysisResult(ModelOption.STANFORD_NLP_DEFAULT.getRepresentation(), result));
		}
		
		return response;
	}
	
	
	@RequestMapping(value="/analysis_bulk", method=RequestMethod.POST)
	public @ResponseBody SentimentAnalysisBulkResponse analyseSentimentBulk(@RequestBody SentimentAnalysisBulkRequest request) {
		List<AnalysisResult> analysisResults = callWorkbookServiceAnalysis(request.getModels(), request.getMessages());
		UUID uuid = workbookService.saveToNewWorkbook(analysisResults);
		return new SentimentAnalysisBulkResponse(uuid, analysisResults);
	}

	
	@RequestMapping(value="/upload_comment_list", method=RequestMethod.POST)
	public @ResponseBody UploadResponse uploadCommentList(@RequestParam("file") MultipartFile file, @RequestParam("models") List<ModelOption> models) {
		String filePath = workbookService.writeAwayUpload(file);
		
		
		if (filePath != null) {
			List<String> messages = workbookService.extractMessagesFromWorkbook(filePath);
			List<AnalysisResult> analysisResults = callWorkbookServiceAnalysis(models, messages);
			UUID uuid = workbookService.saveToExistingWorkbook(filePath, analysisResults);
			return new UploadResponse("success", uuid);
		} else {
			return new UploadResponse("failed", null);
		}
		
	}
	
	
	/**
	 * Helper method.
	 */
	private List<AnalysisResult> callWorkbookServiceAnalysis(List<ModelOption> models, List<String> messages) {
		List<AnalysisResult> results = new ArrayList<AnalysisResult>();
		
		if (models.contains(ModelOption.STANFORD_NLP_DEFAULT)) {
			List<StanfordAnalysisResult> result = stanfordService.analyseBulk(messages);
			results.add(new AnalysisResult(ModelOption.STANFORD_NLP_DEFAULT.getRepresentation(), result));
		}
		
		return results;
	}
	
	
	@RequestMapping(value="/download_latest", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> downloadWorkbook(@RequestParam("id") String id) {
		try {
			String fileName = id + ".xlsx";
			InputStream inputStream = new FileInputStream(WorkbookService.SHEETS_FOLDER + File.separator + fileName);
			byte[] out = IOUtils.toByteArray(inputStream);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
			responseHeaders.add(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			return new ResponseEntity<byte[]>(out, responseHeaders, HttpStatus.OK);
		} catch(FileNotFoundException exception) {
			return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
		} catch(IOException exception) {
			return new ResponseEntity<byte[]>(HttpStatus.valueOf(500));
		}
	}
	
}

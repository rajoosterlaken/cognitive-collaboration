package com.xheory.cc.rest.sentiment.controllers.dto;

import java.util.List;

import com.xheory.cc.rest.sentiment.ModelOption;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SentimentAnalysisBulkRequest {
	
	private List<String> messages;
	private List<ModelOption> models;

}

package com.xheory.cc.rest.sentiment.controllers.dto;

import lombok.Data;

@Data
public class WorkbookDownloadRequest {
	
	private String download_id;
	
}

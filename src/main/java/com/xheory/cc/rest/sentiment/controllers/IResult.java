package com.xheory.cc.rest.sentiment.controllers;

public interface IResult {

	public String getSentence();
	public double[] getCoefficients();
	public int getPredictionIndex();
	
}

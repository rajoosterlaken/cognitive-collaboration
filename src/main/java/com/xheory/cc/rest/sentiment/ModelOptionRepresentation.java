package com.xheory.cc.rest.sentiment;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ModelOptionRepresentation {

	private ModelOption model;
	private String description;
	
}

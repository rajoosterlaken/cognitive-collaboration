package com.xheory.cc.rest.sentiment.controllers.dto;

import java.util.List;

import com.xheory.cc.rest.sentiment.ModelOption;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SentimentAnalysisRequest {
	
	private String message;
	private List<ModelOption> models;
	
}

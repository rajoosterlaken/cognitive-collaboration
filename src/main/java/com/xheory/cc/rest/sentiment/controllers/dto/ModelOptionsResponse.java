package com.xheory.cc.rest.sentiment.controllers.dto;

import java.util.ArrayList;
import java.util.List;

import com.xheory.cc.rest.sentiment.ModelOption;
import com.xheory.cc.rest.sentiment.ModelOptionRepresentation;

import lombok.Getter;

@Getter
public class ModelOptionsResponse {
	
	private static ModelOptionsResponse init = null;
	private List<ModelOptionRepresentation> models = new ArrayList<ModelOptionRepresentation>();
	private Integer amount;
	
	private ModelOptionsResponse() {
		ModelOption[] modelOptions = ModelOption.values();
		for (int i = 0; i < modelOptions.length; i++) {
			models.add(modelOptions[i].getRepresentation());
		}
		amount = models.size();
	}
	
	public static ModelOptionsResponse get() {
		if (init == null) {
			init = new ModelOptionsResponse();
		}
		return init;
	}
	
}

package com.xheory.cc.rest.sentiment.controllers.dto;

import java.util.List;

import com.xheory.cc.rest.sentiment.ModelOptionRepresentation;
import com.xheory.cc.rest.sentiment.controllers.IResult;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnalysisResult {
	
	private ModelOptionRepresentation model;
	private List<? extends IResult> results;
	
}

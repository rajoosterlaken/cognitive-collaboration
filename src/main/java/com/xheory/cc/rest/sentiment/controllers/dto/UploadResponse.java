package com.xheory.cc.rest.sentiment.controllers.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import lombok.Data;

@Data
public class UploadResponse {
	
	private String result;
	private UUID download_id;
	private String date;
	
	
	public UploadResponse(String result) {
		this.result = result;
	}
	
	
	public UploadResponse(String result, UUID download_id) {
		this.result = result;
		this.download_id = download_id;
		this.date = getTimeStamp();
	}

	
	/**
	 * Get current timestamp in "YYYY-MM-DD HH-MM-SS" form.
	 */
	private String getTimeStamp() {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return f.format(new Date());
	}
}

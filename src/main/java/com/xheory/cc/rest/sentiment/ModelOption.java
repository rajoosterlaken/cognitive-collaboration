package com.xheory.cc.rest.sentiment;

import lombok.Getter;

public enum  ModelOption {
	
	STANFORD_NLP_DEFAULT ("Stanford NLP Default");
	
	@Getter
	private String description;
	
	private ModelOption(String description) {
		this.description = description;
	}
	
	public ModelOptionRepresentation getRepresentation() {
		return new ModelOptionRepresentation(this, description);
	}
}
